<!DOCTYPE html> 
<html>
	<head>
		<link rel="canonical" href="https://parallel-launcher.ca/layout/docs" />
		<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon" />
		
		<meta charset="UTF-8">
		
		<title>Advanced Star Layout Documentation</title>
		
		<style>
			body {
				position: relative;
				font-family: sans-serif;
				max-width: 1200px;
				margin: auto;
				padding: 0.5rem;
				padding-bottom: 5rem;
			}
			
			h2 {
				text-decoration: underline;
			}
			
			h3 {
				margin-bottom: 0.5rem;
			}
			
			h4 {
				margin-bottom: 0;
			}
			
			p {
				margin-top: 0;
			}
			
			pre.schema {
				color: black;
				font-family: monospace;
				tab-size: 4;
			}
			
			pre.schema > span {
				color: #707070;
			}
			
			pre.schema > span > b {
				color: black;
			}
			
			div.props {
				color: black;
			}
			
			div.props > b {
				font-family: monospace;
				font-size: 1.25em;
			}
			
			div.props > div {
				margin-left: 4ch;
			}
			
			div.props > div > span {
				font-family: monospace;
			}
			
			div.props > div > a {
				color: #26802f;
				text-decoration: none;
			}
			
			div.props > div > a:hover {
				text-decoration: underline dotted;
			}
			
			a.link {
				color: #0033AA !important;
				text-decoration: underline dotted !important;
			}
			
			#dark-mode {
				position: fixed;
				top: 0;
				left: 0;
				width: 100vw;
				height: 100vh;
				backdrop-filter: invert( 100% );
				z-index: 100;
				pointer-events: none;
			}
			
			#theme-switcher {
				position: relative;
				vertical-align: middle;
				z-index: 101;
				padding-left: 0;
				padding-right: 0;
			}
			
		</style>
	</head>
	<body>
		<div id="dark-mode" hidden></div>
		
		<h1>Parallel Launcher Avanced Star Layout Creation <button id="theme-switcher">🌘</button></h1>
		
		<p>This document decribes how to create and test a custom star layout for your SM64 hack.</p>
		<p><b>This process is intended for romhack developers who have altered how their hack stores save data. If your hack simply uses the vanilla save format, or only uses the 8 star per course patch, then you can instead use the much simpler web UI on romhacking.com to create your star layout.</b></p>
		<p>Advanced star layouts consist of a single json file that tells Parallel Launcher where to look in the save data for star progress and how it should display that information to the user.</p>
		
		<h2>Layout Schema</h2>
		
		<pre class="schema">
{
	"$schema": "https://parallel-launcher.ca/layout/advanced-01/schema.json",
	"format": {
		"save_type": <span>[Optional] (string)</span>,
		"num_slots": <span>(number)</span>,
		"slots_start": <span>(number)</span>,
		"slot_size": <span>(number)</span>,
		"active_bit": <span>(number)</span>,
		"checksum_offset": <span>(number <b>OR</b> null)</span>
	},
	"groups": [
		{
			"name": <span>(string)</span>,
			"side": <span>(string)</span>,
			"courses": [
				{
					"name": <span>[Optional] (string)</span>,
					"data": [
						{
							"offset": <span>(number)</span>,
							"mask": <span>(number)</span>
						},
						...
					]
				},
				...
			]
		},
		...
	],
	"collectedStarIcon": <span>[Optional] (string <b>OR</b> null)</span>,
	"missingStarIcon": <span>[Optional] (string <b>OR</b> null)</span>
}
		</pre>
		
		<h3 id="s-root">Top Level Properties</h3>
		<div class="props">
			<b>$schema</b> - string, requried
			<div>The schema uri. Must be <span>"https://parallel-launcher.ca/layout/advanced-01/schema.json"</span></div>
			<b>format</b> - object, required
			<div>A <a href="#s-format">Format</a> object describing the save type and slot layout</div>
			<b>groups</b> - array, required
			<div>An array of one or more <a href="#s-group">Group</a>s (see below)</div>
			<b>collectedStarIcon</b> - string | null, optional
			<div>A base64-encoded image to use as an icon for collected stars. The image must be under 5kB and will be displayed at 24x24 resolution. Recommended format is SVG or PNG. You can use <a class="link" href="icon-encoder">this web UI</a> to convert an icon into base64. If not present, a default star icon will be used.</div>
			<b>missingStarIcon</b> - string | null, optional
			<div>Same as above, but for the icon shown for uncollected stars. If not present, a grey silhouette of the collected star icon will be used.</div>
		</div>
		
		<h3 id="s-format">Format Properties</h3>
		<div class="props">
			<b>save_type</b> - string, optional
			<div>What type of save data is used. Valid values are <span>EEPROM</span>, <span>SRAM</span>, <span>FlashRAM</span>, <span>MemPak</span>, and <span>Multi</span>. If this property is not present, it defaults to <span>EEPROM</span>. See <a href="#save-types">Save Types</a> for more information.</div>
			<b>num_slots</b> - number, required
			<div>The number of save slots accessible. All slots are assumed to be directly next to each other in the save file.</div>
			<b>slots_start</b> - number, required
			<div>The offset from the start of the save data where the first save slot begins. In the vanilla save format, this has value <span>0</span>. If you are using the <span>MemPak</span> save type and your total save file size is greater than 32kB, then this must have value <span>0</span>.</div>
			<b>slot_size</b> - number, required
			<div>The size of each save slot in bytes. In the vanilla save format, this has value <span>112</span>. If you are using the <span>MemPak</span> save type and your total save file size is greater than 32kB, then this must have value <span>32768</span>.</div>
			<b>active_bit</b> - number, required
			<div>The offset from the start of the save slot in bits of the bit that indicates whether a save slot exists or not. In the vanilla format, this has value <span>95</span>.</div>
			<b>checksum_offset</b> - number | null, required
			<div>The offset from the start of the save slot where the checksum is located. In the vanilla save format, this has value 54. If you have removed the checksum from your save format, enter <span>null</span> for this value.</div>
		</div>
		
		<h3 id="s-group">Group Properties</h3>
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the group to show in Parallel Launcher.</div>
			<b>side</b> - string, required
			<div>One of <span>"left"</span>, <span>"right"</span>, <span>"top"</span>, or <span>"bottom"</span> indicating which side of the UI the group should appear on. Groups in the top or bottom sides will span both the left and right columns.</div>
			<b>courses</b> - array, required
			<div>An array of <a href="#s-course">Course</a>s to list in the group. Usually, this represents a course, but it can be any collection of star data. This array can be empty if you simply want to use the group to display a line of text.</div>
		</div>
		
		<h3 id="s-course">Course Properties</h3>
		<div class="props">
			<b>name</b> - string, optional
			<div>The label to show in front of the stars. If this property is not present, no label will be shown.</div>
			<b>data</b> - array, required
			<div>An array of <a href="#s-data">Star Data</a>, each representing a single byte of save data. Must contain at least one entry. All stars in a course will be shown on the same line. To show stars on multiple lines, add another <a href="#s-course">Course</a> with an empty name below this one instead. Note that the stars in each datum will be in order of the least significant bit on the left, and the most significant bit on the right, opposite to how the number would be written in binary. This is to make it consistent with how star data is stored in vanilla.</div>
		</div>
		
		<h3 id="s-data">Star Data Properties</h3>
		<div class="props">
			<b>offset</b> - number, required
			<div>The offset of the byte from the start of the save slot</div>
			<b>mask</b> - number, required
			<div>A mask of which bits in the byte contain star data. For example, <span>255</span> means all bits contain star data, while <span>1</span> means only the least significant bit should be checked. Cannot be <span>0</span>.</div>
		</div>
		
		<h3 id="save-types">Save Types</h3>
		<p>The following save types are supported:</p>
		
		<h4>EEPROM</h4>
		<p>The most common save format, and the kind used for vanilla SM64. Maximum save file size is 2kB (2048 bytes), but if you use more than 512 bytes, your hack may require some <a class="link" href="http://krikzz.com/pub/support/everdrive-64/x-series/everdrive-64-manual.pdf">configuration</a> to work on Everdrive.</p>
		
		<h4>SRAM</h4>
		<p>A larger save format with a maximum size of 32kB (32768 bytes). May require some <a class="link" href="http://krikzz.com/pub/support/everdrive-64/x-series/everdrive-64-manual.pdf">configuration</a> to work on Everdrive.</p>
		
		<h4>FlashRAM</h4>
		<p>The largest save format available with a maximum size of 128kB (131072 bytes). May require some <a class="link" href="http://krikzz.com/pub/support/everdrive-64/x-series/everdrive-64-manual.pdf">configuration</a> to work on Everdrive.</p>
		
		<h4>MemPak</h4>
		<p>A save format that, on console, stores data in the MemPak addon for your N64 controller. This means you cannot use both MemPak saves and rumble for the same hack. Each MemPak can hold 32kB (32768 bytes), for a total of 128kB (131072 bytes) if all four controllers are used.</p>
		
		<h4>Multi</h4>
		<p>This mode operates on RetroArch's raw save file (SRM), allowing you to access all of the above save formats at once. There is very little reason to actually do this. In this mode, your offsets should be given from the start of the SRM save file. The SRM save format is simply all 2kB of EEPROM, followed by all 4 MemPaks in order, followed by SRAM, followed by FlashRAM.</p>
		
		<h2>Testing Your Layout</h2>
		
		<p>You can test the basic syntax of your layout using any JSON schema validator such as <a class="link" href="https://www.jsonschemavalidator.net/">this one</a>. The schema to validate against can be found <a class="link" href="https://parallel-launcher.ca/layout/advanced-01/schema.json">here</a>.</p>
		
		<p>To test that your layout is reading save data correctly, you can use the star layout tester in Parallel Launcher. In the classic view, right click on the rom you want to test your layout on (or, if you are in the RHDC view, select the hack and click the &#8801; button beside the version selector), then select <i>[RHDC] Test Star Layout</i>. You can then paste the contents of your layout file into the textbox, and Parallel Launcher will validate it and show you a preview of what it will look like on your current save file. You can also click on the stars to temporarily toggle them in your save file and use the <b>Test Layout</b> button to test your rom with the altered save file to verify that it behaves as expected. Any changes you make to your save file here will be reverted once you close the layout tester dialog, and no progress will be submitted to RHDC, so you can freely test without worrying about breaking your save file or star progress.</p>
		
		<script>
			const themeSwitcher = document.getElementById( 'theme-switcher' );
			const darkMode = document.getElementById( 'dark-mode' );
			
			if( darkMode.style['backdrop-filter'] === undefined ) {
				themeSwitcher.hidden = true;
			} else if( localStorage.getItem( 'dark-mode' ) ) {
				darkMode.hidden = false;
				themeSwitcher.textContent = '🌞';
			}
			
			themeSwitcher.addEventListener( 'click', function() {
				if( darkMode.hidden ) {
					darkMode.hidden = false;
					themeSwitcher.textContent = '🌞';
					localStorage.setItem( 'dark-mode', '1' );
				} else {
					darkMode.hidden = true;
					themeSwitcher.textContent = '🌘';
					localStorage.removeItem( 'dark-mode' );
				}
			});
		</script>
		
	</body>
</html>
